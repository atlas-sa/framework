#include "PathResolver/PathResolver.h"
#include <TF1.h>
#include <TFile.h>
#include <TH2.h>
#include <TMath.h>
#include <TRandom.h>

class SoftBtagger {
public:
  SoftBtagger(const char *name) {
    TString effFile = PathResolverFindCalibFile(
        "SimpleAnalysisCodes/SoftBtagEfficiency.root");
    TFile fh = TFile(effFile);
    effHist = dynamic_cast<TH2 *>(fh.FindObjectAny(name)->Clone());
    effHist->SetDirectory(0);
    if (!effHist)
      throw std::runtime_error(
          "Could not find efficiency map for soft b-tagging");
  };

  ~SoftBtagger() { delete effHist; };

  double eff(double pt, double eta) {
    return effHist->GetBinContent(effHist->FindBin(pt, fabs(eta)));
  };

  double isTagged(double pt, double eta) {
    return eff(pt, eta) > gRandom->Rndm();
  };

private:
  TH2 *effHist;
};
