#include "SimpleAnalysisFramework/AnalysisClass.h"
#include "SimpleAnalysisFramework/Reader.h"

#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"
#include "fastjet/ClusterSequence.hh"

class HEPMCReader : public Reader {
public:
  HEPMCReader();
  ~HEPMCReader();
  void AddOptions(po::options_description &desc);
  bool isFileType(TFile *fh, po::variables_map &vm);
  void Init(){};

protected:
  HepMC3::ConstGenParticles selectPid(HepMC3::GenEvent &evt,
                                      std::vector<int> pdgIds, int status);
  HepMC3::ConstGenParticlePtr
  findMother(const HepMC3::ConstGenParticlePtr &part);
  void findTauDecayParts(const HepMC3::ConstGenParticlePtr &part,
                         HepMC3::ConstGenParticles &partList);
  bool isIsoType(const HepMC3::ConstGenParticlePtr &parent);
  HepMC3::FourVector dressLepton(const HepMC3::ConstGenParticlePtr &lepton,
                                 const HepMC3::ConstGenParticles &photons);

  void flavorTagType(std::vector<fastjet::PseudoJet> &jets,
                     std::vector<int> &flavors,
                     HepMC3::ConstGenParticles particles, int flavor);
  void flavorTag(HepMC3::GenEvent &evt, std::vector<fastjet::PseudoJet> &jets,
                 std::vector<int> &flavors);

  bool processEvent(HepMC3::GenEvent &evt);
  void processFilesInternal(const std::vector<std::string> &inputNames,
                            unsigned int nevents);

private:
};
