#include <fstream>
#include <string>

#include "HEPMCReader.h"

#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenPdfInfo.h"
#include "HepMC3/GenVertex.h"
#include "HepMC3/ReaderAscii.h"
#include "HepMC3/ReaderAsciiHepMC2.h"

#include "SUSYTools/SUSYCrossSection.h"

#include "fastjet/contrib/EnergyCorrelator.hh"
#include "fastjet/tools/Filter.hh"

#define WARN_ONCE(warning)                                                     \
  do {                                                                         \
    static bool first = true;                                                  \
    if (first)                                                                 \
      std::cout << warning << std::endl;                                       \
    first = false;                                                             \
  } while (0)

DefineReader(HEPMCReader);

HEPMCReader::HEPMCReader() : Reader() {}

HEPMCReader::~HEPMCReader() {}

bool HEPMCReader::isFileType(TFile *fh, po::variables_map &vm) {
  if (fh)
    return false;
  std::ifstream infile(vm["input-files"].as<std::vector<std::string>>()[0]);
  std::string header;
  infile >> header;
  if (header.find("HepMC::Version") != std::string::npos)
    return true;
  return false;
}

void HEPMCReader::AddOptions(po::options_description & /*desc*/) {}

HepMC3::ConstGenParticles HEPMCReader::selectPid(HepMC3::GenEvent &evt,
                                                 std::vector<int> pdgIds,
                                                 int status) {
  HepMC3::ConstGenParticles selected;
  for (const auto &part : evt.particles()) {
    if ((part->status() == status || status == 0) &&
        (std::find(pdgIds.begin(), pdgIds.end(), part->abs_pid()) !=
             pdgIds.end() ||
         pdgIds.size() == 0))
      selected.push_back(part);
  }
  return selected;
}

bool HEPMCReader::isIsoType(const HepMC3::ConstGenParticlePtr &parent) {
  int pid = parent->abs_pid();
  if (pid >= 1000000 && pid < 9000000)
    return true;
  if (pid >= 23 && pid < 100)
    return true;
  if (pid == 6)
    return true;
  return false;
}

HepMC3::ConstGenParticlePtr
HEPMCReader::findMother(const HepMC3::ConstGenParticlePtr &part) {
  int pid = part->pid();
  const auto parents = part->parents();
  if (parents.size() == 0)
    return part; //
  if (parents[0]->pid() == pid)
    return findMother(parents[0]);
  if (parents[0]->abs_pid() == 15)
    return findMother(parents[0]); // in case of taus find parent of that
  return parents[0];
}

void HEPMCReader::findTauDecayParts(const HepMC3::ConstGenParticlePtr &part,
                                    HepMC3::ConstGenParticles &partList) {
  for (const auto &pp : part->children()) {
    int status = pp->status();
    int apid = pp->abs_pid();
    if ((status == 2 || status == 11 || status == 10902) && apid != 111 &&
        apid != 311 && apid != 310 && apid != 130) {
      findTauDecayParts(pp, partList);
      continue;
    }
    if (status != 1 && status != 2)
      continue;
    partList.push_back(pp);
  }
}

HepMC3::FourVector
HEPMCReader::dressLepton(const HepMC3::ConstGenParticlePtr &lepton,
                         const HepMC3::ConstGenParticles &photons) {
  HepMC3::FourVector lep = lepton->momentum();
  HepMC3::FourVector dressed = lepton->momentum();
  for (const auto &ph : photons) {
    if (ph->momentum().delta_r2_eta(lep) > 0.01)
      continue;
    int parent_id = findMother(ph)->abs_pid();
    if (parent_id == 15 || parent_id >= 26)
      continue;
    dressed += ph->momentum();
  }
  return dressed;
}

void HEPMCReader::flavorTagType(std::vector<fastjet::PseudoJet> &jets,
                                std::vector<int> &flavors,
                                HepMC3::ConstGenParticles particles,
                                int flavor) {
  for (const auto &part : particles) {
    if (part->momentum().pt() < 5)
      continue;
    double minDr = 0.3;
    int minJet = -1;
    int idx = 0;
    for (const auto &jet : jets) {
      HepMC3::FourVector jetVector(jet.px(), jet.py(), jet.pz(), jet.E());
      double dr = part->momentum().delta_r_eta(jetVector);
      if (dr < minDr) {
        minJet = idx;
        minDr = dr;
      }
      idx++;
    }
    if (minJet != -1 && flavors[minJet] == 0) {
      flavors[minJet] = flavor;
    }
  }
}

void HEPMCReader::flavorTag(HepMC3::GenEvent &evt,
                            std::vector<fastjet::PseudoJet> &jets,
                            std::vector<int> &flavors) {
  flavors.clear();
  flavors.resize(jets.size(), 0);
  flavorTagType(jets, flavors,
                selectPid(evt, {511, 521, 531, 541, 5122, 5132, 5232, 5332}, 2),
                5);
  flavorTagType(jets, flavors,
                selectPid(evt,
                          {411, 421, 431, 4112, 4122, 4132, 4212, 4222, 4232,
                           4312, 4322, 4332},
                          2),
                4);
  flavorTagType(jets, flavors, selectPid(evt, {15}, 2), 15);
}

bool HEPMCReader::processEvent(HepMC3::GenEvent &evt) {
  int eventNumber = evt.event_number();

  // MET and sum ET calculation
  double sumet = 0;
  double metx = 0;
  double mety = 0;

  for (const auto &part : selectPid(evt, {}, 1)) {
    int apid = part->abs_pid();
    if (apid == 12 || apid == 14 || apid == 16 ||
        apid == 1000022) { // Only neutrinos and neutralino1 are considered
                           // invisible
      metx += part->momentum().px();
      mety += part->momentum().py();
    } else {
      if (apid != 13 &&
          part->momentum().abs_eta() < 5) // Muons and particles outside
                                          // acceptance are not counted in sumet
        sumet += part->momentum().pt();
    }
  }

  // identify process type - Note: this is not very robust as it depends on the
  // truth being recorded by generator
  int mcChannel = 0; // ATLAS internal numbering - should not be used except for
                     // filtered samples
  int mcVetoCode = 0; // Also for internal filters
  int susy_part_id[2] = {0, 0};
  int s_id = 0;
  int susy_process = 0;

  for (const auto &part : evt.particles()) {
    int apid = part->abs_pid();
    if (apid < 1000000 || apid > 3000000)
      continue; // only look at susy particles
    const auto parents = part->parents();
    if (parents.size() == 0) {
      susy_part_id[s_id++] = part->pid();
      if (s_id == 2)
        break;
    } else {
      const auto children =
          parents[0]
              ->children(); // assume that all susy particles are connected to
                            // same parent might not be true for all generators
      for (const auto &child : children) {
        apid = child->abs_pid();
        if (apid < 1000000 || apid > 3000000)
          continue; // only look at susy particles
        susy_part_id[s_id++] = child->pid();
        if (s_id == 2)
          break;
      }
    }
  }
  int maxID =
      std::max(abs(susy_part_id[0]) % 1000000, abs(susy_part_id[1]) % 1000000);
  int minID =
      std::min(abs(susy_part_id[0]) % 1000000, abs(susy_part_id[1]) % 1000000);
  if ((maxID <= 6 || minID > 6) &&
      (maxID <= 4 ||
       minID > 4)) // no final state defined for mixed squark+neutralino
                   // production or light+heavy flavour squark
    susy_process = SUSY::finalState(susy_part_id[0], susy_part_id[1]);

  // Fill in event
  TruthEvent *event = new TruthEvent(sumet, metx, mety);
  event->setChannelInfo(mcChannel, mcVetoCode, susy_process);

  TLorentzVector tlv(0., 0., 0., 0.);
  int idx;

  auto photons = selectPid(evt, {22}, 1);

  // electrons
  idx = 0;
  for (const auto &el : selectPid(evt, {11}, 1)) {
    auto mom = el->momentum();
    mom = dressLepton(el, photons);
    tlv.SetPtEtaPhiM(mom.pt(), mom.eta(), mom.phi(), mom.m());
    const auto mother = findMother(el);
    auto obj = event->addElectron(tlv, -el->pid() / abs(el->pid()),
                                  isIsoType(mother) ? EIsoGood : 0,
                                  mother->pid(), idx++);
    const auto pos = el->production_vertex()->position();
    if (pos.length() > 0.001) {
      obj->setProdVtx(TVector3(pos.x(), pos.y(), pos.z()));
    }
  }
  // muons
  idx = 0;
  for (const auto &mu : selectPid(evt, {13}, 1)) {
    auto mom = mu->momentum();
    mom = dressLepton(mu, photons);
    tlv.SetPtEtaPhiM(mom.pt(), mom.eta(), mom.phi(), mom.m());
    const auto mother = findMother(mu);
    auto obj =
        event->addMuon(tlv, -mu->pid() / abs(mu->pid()),
                       isIsoType(mother) ? MuIsoGood : 0, mother->pid(), idx++);
    const auto pos = mu->production_vertex()->position();
    if (pos.length() > 0.001) {
      obj->setProdVtx(TVector3(pos.x(), pos.y(), pos.z()));
    }
  }

  // taus
  idx = 0;
  for (const auto &tau : selectPid(evt, {15}, 2)) {
    HepMC3::ConstGenParticles decayParts;
    findTauDecayParts(tau, decayParts);
    HepMC3::FourVector visTau;
    bool isHadronicTau = false;
    int numCharged = 0;
    int numNeutral = 0;
    for (const auto &pp : decayParts) {
      int apid = pp->abs_pid();
      if (apid == 12 || apid == 14 || apid == 16)
        continue; // neutrinos
      visTau += pp->momentum();
      if (apid > 100)
        isHadronicTau = true;
      if (apid != 22 && apid != 111 && apid != 311 && apid != 310 &&
          apid != 130)
        numCharged++;
      else
        numNeutral++;
    }
    if (!isHadronicTau)
      continue;

    const auto mother = findMother(tau);

    int tauId = isIsoType(mother) ? TauIsoGood : 0;
    if (numCharged == 3)
      tauId |= TauThreeProng;
    else
      tauId |= TauOneProng;

    if (numNeutral == 0)
      tauId |= TauZeroNeutral;
    else if (numNeutral == 1)
      tauId |= TauOneNeutral;
    else if (numNeutral == 2)
      tauId |= TauTwoNeutral;
    else if (numNeutral == 3)
      tauId |= TauThreeNeutral;
    else if (numNeutral == 4)
      tauId |= TauFourNeutral;
    else
      tauId |= TauFiveNeutral;

    tlv.SetPtEtaPhiM(visTau.pt(), visTau.eta(), visTau.phi(), visTau.m());
    event->addTau(tlv, -tau->pid() / abs(tau->pid()), tauId, mother->pid(),
                  idx++);
  }

  // photons
  idx = 0;
  for (const auto &photon : photons) {
    if (photon->momentum().pt() < 20)
      continue;
    tlv.SetPtEtaPhiM(photon->momentum().pt(), photon->momentum().eta(),
                     photon->momentum().phi(), 0);
    const auto mother = findMother(photon);
    event->addPhoton(tlv, isIsoType(mother) ? PhotonIsoGood : 0, mother->pid(),
                     idx++);
  }

  double gen_ht = 0; // Only relevant for filtered samples - shouldn't be the
                     // case for hepmc samples
  double gen_met = 0;
  event->setGenHT(gen_ht);
  event->setGenMET(gen_met);

  // AntiKt 0.4 jets (AntiKt4TruthDressedWZJets in ATLAS)
  idx = 0;

  std::vector<fastjet::PseudoJet> antikt4_part;
  for (const auto &part : selectPid(evt, {}, 1)) {
    int apid = part->abs_pid();
    if (apid > 1000000 && apid < 9000000)
      continue; // don't include SUSY particles
    const auto mother = findMother(part);
    if (apid > 10 && apid < 17 && isIsoType(mother))
      continue; // do not include prompt leptons
    if (part->momentum().abs_eta() > 5)
      continue;
    // FIXME: missing exclusion of photons used for dressing prompt leptons
    fastjet::PseudoJet pj(part->momentum().px(), part->momentum().py(),
                          part->momentum().pz(), part->momentum().e());
    antikt4_part.push_back(pj);
  }
  fastjet::JetDefinition antikt4_def(fastjet::antikt_algorithm, 0.4);
  fastjet::ClusterSequence clust_seq(antikt4_part, antikt4_def);
  std::vector<fastjet::PseudoJet> antikt4_jets =
      sorted_by_pt(clust_seq.inclusive_jets(5));

  std::vector<int> jetFlavor;

  flavorTag(evt, antikt4_jets, jetFlavor);

  int jj = 0;
  for (const auto &jet : antikt4_jets) {
    tlv.SetPtEtaPhiM(jet.pt(), jet.eta(), jet.phi(), jet.m());
    int flavor = jetFlavor[jj++];
    int id = (flavor == 5) ? GoodBJet : GoodJet;
    if (flavor == 4)
      id |= TrueCJet;
    else if (flavor == 5)
      id |= TrueBJet;
    else if (flavor == 15)
      id |= TrueTau;
    else
      id |= TrueLightJet;
    event->addJet(tlv, id, idx++);
  }

  // AntiKt10TruthTrimmedPtFrac5SmallR20Jets

  idx = 0;

  std::vector<fastjet::PseudoJet> antikt10_part;
  for (const auto &part : selectPid(evt, {}, 1)) {
    int apid = part->abs_pid();
    if (apid > 1000000 && apid < 9000000)
      continue; // don't include SUSY particles
    if ((apid > 11 && apid < 15) || (apid == 16))
      continue; // do not include muons or neutrinos
    if (part->momentum().abs_eta() > 5)
      continue;
    fastjet::PseudoJet pj(part->momentum().px(), part->momentum().py(),
                          part->momentum().pz(), part->momentum().e());
    antikt10_part.push_back(pj);
  }
  fastjet::JetDefinition antikt10_def(fastjet::antikt_algorithm, 1.0);
  fastjet::ClusterSequence clust_seq10(antikt10_part, antikt10_def);
  std::vector<fastjet::PseudoJet> antikt10_jets =
      sorted_by_pt(clust_seq10.inclusive_jets(50));

  fastjet::Filter trimmer(fastjet::JetDefinition(fastjet::kt_algorithm, 0.2),
                          fastjet::SelectorPtFractionMin(0.05));

  fastjet::contrib::EnergyCorrelator ecf1corr(
      1, 1.0, fastjet::contrib::EnergyCorrelator::pt_R);
  fastjet::contrib::EnergyCorrelator ecf2corr(
      2, 1.0, fastjet::contrib::EnergyCorrelator::pt_R);
  fastjet::contrib::EnergyCorrelator ecf3corr(
      3, 1.0, fastjet::contrib::EnergyCorrelator::pt_R);

  jj = 0;
  for (const auto &ojet : antikt10_jets) {
    fastjet::PseudoJet jet = trimmer(ojet);
    tlv.SetPtEtaPhiM(jet.pt(), jet.eta(), jet.phi(), jet.m());
    HepMC3::FourVector jetVector(jet.px(), jet.py(), jet.pz(), jet.E());

    float fatJetD2 = -999;

    float ecf1 = ecf1corr(jet);
    float ecf2 = ecf2corr(jet);
    float ecf3 = ecf3corr(jet);
    // calculate D2
    if (fabs(ecf2) > 1e-8)
      fatJetD2 = ecf3 * pow(ecf1, 3.0) / pow(ecf2, 3.0);

    int tagid = 0;

    // Match to hadronic W/Z
    for (const auto &boson : selectPid(evt, {23, 24}, 0)) {
      if (boson->momentum().pt() < 150)
        continue;
      bool hadBoson = true;
      for (const auto &child : boson->children()) {
        if (child->pid() == boson->pid()) {
          hadBoson = false;
          break;
        }
        if (child->abs_pid() >= 11 && child->abs_pid() <= 18) {
          hadBoson = false;
          break;
        }
      }
      if (!hadBoson)
        continue;
      if (jetVector.delta_r_eta(boson->momentum()) < 0.75) {
        if (boson->abs_pid() == 24)
          tagid |= WTag;
        else if (boson->abs_pid() == 23)
          tagid |= ZTag;
        break;
      }
    }

    // Count b-quarks
    int nb_match_fatjet = 0;
    for (const auto &bquark : selectPid(evt, {5}, 0)) {
      if (bquark->momentum().pt() < 15)
        continue;
      bool atend = true;
      for (const auto &child : bquark->children()) {
        if (child->pid() == bquark->pid()) {
          atend = false;
          break;
        }
      }
      if (!atend)
        continue;
      if (jetVector.delta_r_eta(bquark->momentum()) < 1.0)
        nb_match_fatjet++;
    }
    switch (nb_match_fatjet) {
    case 0:
      tagid |= ZeroBtagSubjet;
      break;
    case 1:
      tagid |= OneBtagSubjet;
      break;
    case 2:
      tagid |= TwoBtagSubjet;
      break;
    default:
      tagid |= ManyBtagSubjet;
      break;
    }
    event->addFatJet(tlv, (int)(fatJetD2 * 1e5), tagid, idx++);
  }

  // AntiKtVR30Rmax4Rmin02TruthChargedJets
  // FIXME: to be added later - not currently part of ATLAS standard Truth3
  // samples either

  // BHadron

  idx = 0;
  for (const auto &bhad :
       selectPid(evt, {511, 521, 531, 541, 5122, 5132, 5232, 5332}, 2)) {
    if (bhad->momentum().pt() < 2)
      continue;
    tlv.SetPtEtaPhiM(bhad->momentum().pt(), bhad->momentum().eta(),
                     bhad->momentum().phi(), bhad->momentum().m());
    event->addBHadron(tlv, 0, idx++);
  }

  // addHSTruth
  idx = 0;
  for (const auto &part : evt.particles()) {
    int apid = part->abs_pid();
    if ((apid > 1000000 && apid < 9000000) || apid == 12 || apid == 14 ||
        apid == 16 || apid == 5 || apid == 6 ||
        (apid > 22 &&
         apid < 26)) { // SUSY particles, neutrinos, b/t-quarks, W/Z/h bosons
      int pdgId = part->pid();
      int status = part->status();
      if (apid == 5 || apid == 6) {
        bool inRadiativeChain = false;
        for (const auto &child : part->children()) {
          if (child->pid() == pdgId)
            inRadiativeChain = true;
        }
        if (inRadiativeChain)
          continue;
      }

      int id = abs(pdgId) | (status << 24);
      if (pdgId < 0)
        id |= 1 << 31;
      if (apid > 22 && apid < 26) {
        bool leptonic = false;
        for (const auto &child : part->children()) {
          if (child->abs_pid() >= 11 && child->abs_pid() <= 18)
            leptonic = true;
        }
        if (leptonic)
          id |= 1 << 30;
      }
      tlv.SetPtEtaPhiM(part->momentum().pt(), part->momentum().eta(),
                       part->momentum().phi(), part->momentum().m());
      const auto mother = findMother(part);
      event->addHSTruth(tlv, 0, id, mother->pid(), idx++);
    }
  }

  // PDF and event weight information

  const auto pdfInfo = evt.pdf_info();
  if (pdfInfo) {
    // store information about the HepMC::PdfInfo if it exists
    event->setPDFInfo(pdfInfo->parton_id[0], pdfInfo->x[0], pdfInfo->xf[0],
                      pdfInfo->parton_id[1], pdfInfo->x[1], pdfInfo->xf[1],
                      pdfInfo->scale);
  } else {
    WARN_ONCE("No PDF information available");
    event->setPDFInfo(0, 0, 0, 0, 0, 0, 0);
  }

  std::vector<float> weights;
  for (const auto weight : evt.weights()) {
    weights.push_back(weight);
  }
  event->setMCWeights(weights);

  _analysisRunner->processEvent(event, eventNumber);
  delete event;

  return true;
}

void HEPMCReader::processFilesInternal(
    const std::vector<std::string> &inputFileNames, unsigned int nevents) {
  unsigned int procEvents = 0;

  for (const auto &inName : inputFileNames) {
    std::cout << "Processing: " << inName << std::endl;
    HepMC3::ReaderAsciiHepMC2 input_file(inName.c_str());
    while (true) {
      ++procEvents;
      if (procEvents > nevents)
        break;
      HepMC3::GenEvent evt(HepMC3::Units::GEV, HepMC3::Units::MM);
      input_file.read_event(evt);
      if (input_file.failed())
        break;
      processEvent(evt);
    }
    if (procEvents > nevents)
      break;
  }
}
