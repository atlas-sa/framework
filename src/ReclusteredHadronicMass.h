#ifndef MYANALYSIS_RECLUSTEREDHADRONICMASS_H_
#define MYANALYSIS_RECLUSTEREDHADRONICMASS_H_
#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <fastjet/ClusterSequence.hh>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

class ReclusteredHadronicMass {

public:
  ReclusteredHadronicMass();
  ~ReclusteredHadronicMass();
  std::pair<double, TLorentzVector> applyReclustering(AnalysisObjects list_jets,
                                                      double candMass) const;

  double R0 = 3.0;
  double Rup = 0.3;
  double Rdown = 0.5;
  double fRec = 0.5;

  bool limitShrink = true;

  bool varRdown = false;
  double varRdown_r = 0.15;
  double varRdown_min = 0.2;
  double varRdown_max = 0.5;

private:
  std::vector<fastjet::PseudoJet>
  SelectInitialJets(AnalysisObjects list_jets) const;
  std::pair<bool, fastjet::PseudoJet>
  RecursiveRecluster(const fastjet::PseudoJet &candidate, double candRadius,
                     size_t step, double candMass) const;

  static double Radius(double pT, double mass) { return (2 * mass) / pT; }
  double GetMaxRadius(double pTDepRadius, double currentR) const {
    if (varRdown && currentR > 0) {
      const double rd =
          std::min(varRdown_max, std::max(varRdown_r * currentR, varRdown_min));
      return pTDepRadius + rd;
    }

    return pTDepRadius + Rdown;
  }

  double GetReclusterRadius(double pT, double mass) const {
    return Radius(pT, mass) + fRec * Rdown;
  }
};

#endif
