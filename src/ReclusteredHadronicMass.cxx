#include "src/ReclusteredHadronicMass.h"
#include "TVector3.h"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdlib.h>
#include <vector>

/*
    #####################################################################################
    The code returns a hadronic decaying candidate information once the signal
   jets and the candidate mass are provided to the applyReclustering function.
    #####################################################################################

    Example, for a W boson decaying hadronically:

        ReclusteredHadronicMass reclustering;
        std::pair<double, TLorentzVector> WCand =
   reclustering.applyReclustering(signalJets, 80); reclusteredW_Mass =
   std::get<1>(WCand).M();

*/

ReclusteredHadronicMass::ReclusteredHadronicMass() {}

ReclusteredHadronicMass::~ReclusteredHadronicMass() {}

std::vector<fastjet::PseudoJet>
ReclusteredHadronicMass::SelectInitialJets(AnalysisObjects list_jets) const {
  // Select initial jets for reclustering
  std::vector<fastjet::PseudoJet> initialJets;
  for (size_t i = 0; i < list_jets.size(); ++i) {
    const auto &jet = list_jets[i];

    fastjet::PseudoJet smallJet(jet.Px(), jet.Py(), jet.Pz(), jet.E());
    smallJet.set_user_index(i);
    initialJets.push_back(smallJet);
  }

  return initialJets;
}

std::pair<double, TLorentzVector>
ReclusteredHadronicMass::applyReclustering(AnalysisObjects list_jets,
                                           double candMass) const {

  std::vector<fastjet::PseudoJet> initialJets = SelectInitialJets(list_jets);

  // Initial reclustering of the signal jets with a radius of R0 = 3.0

  fastjet::JetDefinition jetDef(fastjet::antikt_algorithm, R0);
  fastjet::ClusterSequence cs(initialJets, jetDef);
  auto candidates = fastjet::sorted_by_pt(cs.inclusive_jets());

  // Final candidates with reclustering decision
  std::vector<std::pair<bool, fastjet::PseudoJet>> selectedJets;
  std::vector<std::pair<bool, fastjet::PseudoJet>> badJets;

  for (const auto &cand : candidates) {

    // std::cout << "Starting reclustring ..." << std::endl;

    // Initial Reclustering with R0 = 3.0 radius in step 0
    std::pair<bool, fastjet::PseudoJet> clusteredJet =
        RecursiveRecluster(cand, R0, 0, candMass);

    if (std::get<0>(clusteredJet)) {
      selectedJets.push_back(clusteredJet);
    } else {
      badJets.push_back(clusteredJet);
    }
  }

  // Return the reclustered jet mass
  std::vector<std::tuple<double, double, double>> mass;

  TLorentzVector v(0, -9, -9, 0);
  // Select the candiate that has mass closer to the candidate mass
  if (selectedJets.size() == 0) {
    return std::make_pair(0, v);
  } else {
    for (std::pair<bool, fastjet::PseudoJet> clusteredJet : selectedJets) {
      mass.push_back(
          std::make_tuple(abs(std::get<1>(clusteredJet).m() - candMass),
                          std::get<1>(clusteredJet).m(),
                          std::get<1>(clusteredJet)
                              .associated_cluster_sequence()
                              ->jet_def()
                              .R()));
    }

    if (selectedJets.size() > 1)
      std::sort(mass.begin(), mass.end());
    std::tuple<double, double, double> m = mass.front();

    for (std::pair<bool, fastjet::PseudoJet> clusteredJet : selectedJets) {
      if (std::get<1>(clusteredJet).m() == std::get<1>(m)) {
        v.SetPtEtaPhiM(
            std::get<1>(clusteredJet).pt(), std::get<1>(clusteredJet).eta(),
            std::get<1>(clusteredJet).phi(), std::get<1>(clusteredJet).m());
      }
    }
    return std::make_pair(std::get<2>(m), v);
  }
}
// This is the central part of the algorithm.
std::pair<bool, fastjet::PseudoJet>
ReclusteredHadronicMass::RecursiveRecluster(const fastjet::PseudoJet &candidate,
                                            double candRadius, size_t step,
                                            double candMass) const {

  // Calculate pT-dependent radius R(pT) = 2*M/pt
  const double Ri = Radius(candidate.pt(), candMass);

  double Rnew = 0.0;

  // Discard the candidate if R(pT) > R0 +Rup
  if ((Ri - Rup) > candRadius) {
    return std::make_pair(false, candidate);
  }

  // if not, recluster again
  else if (GetMaxRadius(Ri, candRadius) < candRadius) {

    Rnew = limitShrink ? std::max(GetMaxRadius(Ri, candRadius), candRadius / 2.)
                       : GetMaxRadius(Ri, candRadius);

    if (step >= 10) {
      return std::make_pair(false, candidate);
    }

    fastjet::JetDefinition jetDef(fastjet::antikt_algorithm, Rnew);
    auto cs = new fastjet::ClusterSequence(candidate.constituents(), jetDef);

    std::vector<fastjet::PseudoJet> reclusteredJets;
    reclusteredJets = fastjet::sorted_by_pt(cs->inclusive_jets());

    if (reclusteredJets.size() == 0) {
      delete cs;
      return std::make_pair(false, fastjet::PseudoJet());
    }

    cs->delete_self_when_unused();
    auto newCandidate = reclusteredJets[0];

    return RecursiveRecluster(newCandidate, Rnew, step + 1, candMass);
  }
  // Select the candidate
  else {
    return std::make_pair(true, candidate);
  }
}
